//Bank elements
const balanceEl = document.getElementById("balance");
const outstandingLoanEl = document.getElementById("outstandingLoan");
const loanButton = document.getElementById("loanButton");

//Number formatting to currency
const numToNok = (num) => {
  return new Intl.NumberFormat("no-NO", {
    style: "currency",
    currency: "NOK",
    currencyDisplay: "narrowSymbol",
  }).format(num);
};

let balance = 200;
let outstandingLoan = 0;
let activeLoan = false;

balanceEl.innerHTML = `Balance: ${numToNok(balance)}`;
outstandingLoanEl.innerHTML = `Loan: ${numToNok(outstandingLoan)}`;

//Work elements
const payEl = document.getElementById("pay");
const bankButton = document.getElementById("bankPayButton");
const workingButton = document.getElementById("workingButton");
const payLoanButton = document.getElementById("loanPayButton");

let payBalance = 0;
let rate = 100;

//Laptop selection elements
const laptopsDropdownEl = document.getElementById("laptopsDropdown");
const laptopsFeaturesEl = document.getElementById("laptopFeatures");

//LaptopAPI fetching
let laptopsData = [];

fetch("https://hickory-quilled-actress.glitch.me/computers")
  .then((response) => response.json())
  .then((data) => (laptops = data))
  .then((data) => (laptopsData = data))
  .then((laptops) => addLaptopsToSelect(laptops))
  .catch((error) => console.error("Error: ", error.message));

const addLaptopsToSelect = (laptops) => {
  laptops.forEach((laptop) => convertLaptopToOption(laptop));
};
const convertLaptopToOption = (laptop) => {
  const optionElement = document.createElement("option");
  optionElement.text = laptop.title;
  laptopsDropdownEl.appendChild(optionElement);
};

//Laptop info elements
const chosenLaptopImage = document.getElementById("chosenLaptopImage");
const chosenLaptopTitle = document.getElementById("chosenLaptopTitle");
const chosenLaptopDescription = document.getElementById(
  "chosenLaptopDescription"
);
const chosenLaptopPrice = document.getElementById("chosenLaptopPrice");
const buyNowButton = document.getElementById("buyNowButton");

let laptopPrice = 0;
let laptopTitle = "";

const handleLaptopsDropdownChange = (e) => {
  let li;
  laptopsFeaturesEl.innerHTML = "";
  selectedLaptop = laptopsData[e.target.selectedIndex - 1];
  for (const key in selectedLaptop.specs) {
    li = document.createElement("li");
    li.appendChild(document.createTextNode(selectedLaptop.specs[key]));
    laptopsFeaturesEl.appendChild(li);
  }
  chosenLaptopTitle.innerText = selectedLaptop.title;
  chosenLaptopDescription.innerText = selectedLaptop.description;
  chosenLaptopPrice.innerText = numToNok(selectedLaptop.price);
  laptopPrice = Number(selectedLaptop.price);
  laptopTitle = selectedLaptop.title;
  console.log(laptopPrice);
  chosenLaptopImage.src =
    "https://hickory-quilled-actress.glitch.me/" + selectedLaptop.image;
};
function buyComputerFunction() {
  if (balance >= laptopPrice) {
    window.alert(
      `You are now the new proud owner of a ${laptopTitle} laptop 💻`
    );
    balance = balance - laptopPrice;
    balanceEl.innerHTML = `Balance: ${numToNok(balance)}`;
  } else {
    window.alert("You cannot afford this laptop!! 😡");
  }
}

laptopsDropdown.addEventListener("change", handleLaptopsDropdownChange);
buyNowButton.addEventListener("click", buyComputerFunction);

//Work functionality
function workFunction() {
  payBalance += rate;
  payEl.innerText = `Pay: ${numToNok(payBalance)}`;
}
function bankingFunction() {
  let amount = 0;
  if (activeLoan === true) {
    amount = (10 / 100) * payBalance;
    balance += payBalance - amount;
    outstandingLoan += amount;
    payBalance = 0;
    outstandingLoanEl.innerHTML = `Loan: ${numToNok(outstandingLoan)}`;
  } else {
    balance += Number(payBalance);
    payBalance = 0;
  }
  balanceEl.innerHTML = `Balance: ${numToNok(balance)}`;
  payEl.innerText = `Pay: ${numToNok(payBalance)}`;
}
function payLoanFunction() {
  let restAmount = 0;
  if (payBalance >= outstandingLoan) {
    restAmount = Math.abs(outstandingLoan - payBalance);
    balance += Number(restAmount);
    payBalance = 0;
    outstandingLoan = 0;
    outstandingLoanEl.hidden = true;
    payLoanButton.hidden = true;
    if (outstandingLoan === 0) {
      activeLoan = false;
    }
  } else {
    outstandingLoan = outstandingLoan - payBalance;
    outstandingLoanEl.innerHTML = `Loan: ${numToNok(outstandingLoan)}`;
    payBalance = 0;
    payEl.innerText = `Pay: ${numToNok(payBalance)}`;
    if (outstandingLoan === 0) {
      activeLoan = false;
    }
  }
  balanceEl.innerHTML = `Balance: ${numToNok(balance)}`;
  payEl.innerText = `Pay: ${numToNok(payBalance)}`;
}
workingButton.addEventListener("click", workFunction);
bankButton.addEventListener("click", bankingFunction);
payLoanButton.addEventListener("click", payLoanFunction);

//Bank functionality
function loanFunction() {
  let attemptedLoan = Number(window.prompt("Enter amount you want to lend:"));
  if (attemptedLoan <= 0 || isNaN(attemptedLoan)) {
    window.alert("No loan granted ... ✌️");
  } else {
    if (attemptedLoan > balance * 2) {
      window.alert("You cant afford a loan of this size!! 😡");
    } else if (activeLoan === false) {
      window.alert("Loan granted 😄");
      outstandingLoan = attemptedLoan;
      balance += attemptedLoan;
      balanceEl.innerHTML = `Balance: ${numToNok(balance)}`;
      outstandingLoanEl.innerHTML = `Loan: ${numToNok(outstandingLoan)}`;
      outstandingLoanEl.removeAttribute("hidden");
      payLoanButton.removeAttribute("hidden");
      activeLoan = true;
    } else {
      window.alert("You already have a loan 🤔");
    }
  }
}
loanButton.addEventListener("click", loanFunction);
