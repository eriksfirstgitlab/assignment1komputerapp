# Assignment 1 - Dynamic Webpage using JavaScript

First assignment in the frontend part of the Noroff 

## Description

### Part one - HTML & CSS
In the first part of this assignment i created a HTML file that i am going to manipulate in the next part
and styling with CSS so that the application would look nice.

### Part two - JavaScript
In this part of the assignment i was given some requirements in form of three parts.
A bank section, a work section and a laptop section. In the bank section the requirements was to be able to store funds and make bank loans. In the work section you should be able to increase earnings and deposit money into the bank section, in this section you should also be able to pay back the loan taken in the bank section. The last part of the requirements was a laptop section where information would change depending on what computer is chosen. This information comes from this [Web API](https://hickory-quilled-actress.glitch.me/computers).

## Project status
All of the requirements are implemented. There is some more data from the Web API that could be used for more functionality in the webpage.

## Usage
The assignment is run via a web browser with the html, css and js files. The Webpage is also hosted via netlify: [Komputerapperik](https://komputerapperik.netlify.app/) [![Netlify Status](https://api.netlify.com/api/v1/badges/a3774ad0-3d45-4db5-8c5e-43842a210703/deploy-status)](https://app.netlify.com/sites/komputerapperik/deploys)

## Technologies
* HTML
* CSS
* JavaScript

## Sample Wireframe for the Komputer store
A given Wireframe of how the app is going to look
![wireframe](https://gitlab.com/eriksfirstgitlab/assignment1komputerapp/uploads/8d9552f7c0245fd02e61250515f45fbc/wireframe.png)

## Contributers
* Erik Aardal

### License
[MIT](https://choosealicense.com/licenses/mit/)